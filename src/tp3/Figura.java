/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

/**
 *
 * @author fabio
 */
public abstract class Figura {

    private String corPreenchimento;

    public Figura() {
        this("Cinzento");
    }

    public Figura(String corPreenchimento) {
        setCorPreenchimento(corPreenchimento);
    }

    public String getCorPreenchimento() {
        return corPreenchimento;
    }

    public void setCorPreenchimento(String corPreenchimento) {
        if (corPreenchimento.isEmpty()) {
            this.corPreenchimento = "Cinzento";
        } else {
            this.corPreenchimento = corPreenchimento;
        }
    }

    @Override
    public String toString() {
        return "Cor: " + corPreenchimento;
    }

    public abstract double area();
}
