/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

/**
 *
 * @author fabio
 */
public class Retangulo extends Figura implements Cloneable{

    private double comprimento;
    private double largura;

    public Retangulo() {
        this(2, 1);
    }

    public Retangulo(double comprimento, double largura) {
        this(comprimento, largura, "");
    }

    public Retangulo(double comprimento, double largura, String corPreenchimento) {
        super(corPreenchimento);
        setComprimento(comprimento);
        setLargura(largura);
    }

    public Retangulo(Retangulo retangulo) {
        this(retangulo.getComprimento(), retangulo.getLargura(), retangulo.getCorPreenchimento());
    }

    public double getComprimento() {
        return comprimento;
    }

    public double getLargura() {
        return largura;
    }

    public void setComprimento(double comprimento) {
        if (comprimento < 2) {
            this.comprimento = 2;
        } else {
            this.comprimento = comprimento;
        }
    }

    public void setLargura(double largura) {
        if (largura < 1) {
            this.largura = 1;
        } else {
            this.largura = largura;
        }
    }

    @Override
    public String toString() {
        return "Retângulo de comprimento " + this.comprimento + " , largura "
                + this.largura + " " + super.toString();
    }

    @Override
    public double area() {
        return comprimento * largura;
    }

    @Override
    protected Retangulo clone() throws CloneNotSupportedException {
        Retangulo cloned = (Retangulo) super.clone();
        cloned.setComprimento(this.getComprimento());
        cloned.setCorPreenchimento(this.getCorPreenchimento());
        cloned.setLargura(this.getLargura());
        return cloned;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Retangulo)) {
            return false;
        }
        if (!(this.comprimento == ((Retangulo) obj).getComprimento())) {
            return false;
        }
        if (!(this.largura == ((Retangulo) obj).getLargura())) {
            return false;
        }
        return true;
    }
}
