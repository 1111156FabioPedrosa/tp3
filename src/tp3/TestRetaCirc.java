/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.util.ArrayList;

/**
 *
 * @author fabio
 */
public class TestRetaCirc {

    public static void main(String[] args) throws CloneNotSupportedException {
        System.out.println("Teste class Círculo:");
        Circulo c1 = new Circulo(-5, "");
        System.out.println(c1);
        c1.setCorPreenchimento("Laranja");
        c1.setRaio(5);
        System.out.println(c1);
        System.out.println("Raio: " + c1.getRaio() + " cor: " + c1.getCorPreenchimento());

        Circulo c2 = new Circulo(3, "Azul");
        System.out.println(c2);

        System.out.println("\nTeste Retângulo:");
        Retangulo r1 = new Retangulo(1, 0);
        System.out.println(r1);
        r1.setComprimento(7);
        r1.setCorPreenchimento("Preto");
        r1.setLargura(9);
        System.out.println(r1);

        Retangulo r2 = new Retangulo(3, 12, "Verde");
        System.out.println(r2);

        System.out.println("\nArrayList:");

        ArrayList figuras = new ArrayList();
        figuras.add(r2);
        figuras.add(c1);
        figuras.add(c2);
        figuras.add(r1);

        for (Object figura : figuras) {
            System.out.println(figura);
        }

        System.out.println("\nTestar cálculo das áreas:");
        for (Object figura : figuras) {
            System.out.println(figura + " Area: " + ((Figura) figura).area());
        }
        
        System.out.println("\nListar instânsias de Retângulo:");
        for (Object figura: figuras) {
            if (figura instanceof Retangulo) {
                System.out.println(figura);
            }
        }
        
        System.out.println("\nListar instânsias de Círculo:");
        for (Object figura: figuras) {
            if (figura instanceof Circulo) {
                System.out.println(figura);
            }
        }
        
        System.out.println("\nTest Equals, clone:");
        System.out.println("Círculo:");
        System.out.println(c1.equals(c2));
        Circulo c3 = new Circulo(c1);
        System.out.println(c1.equals(c3));
        System.out.println("Retângulo:");
        System.out.println(r1.equals(r2));
        Retangulo r3 = new Retangulo(r1);
        System.out.println(r1.equals(r3));
        
        System.out.println("\nTest Clone:");
        Retangulo r4 = new Retangulo(r1);
        System.out.println(r1);
        System.out.println(r4);
        Retangulo r5 = r2.clone();
        System.out.println(r2);
        System.out.println(r5);
        Circulo c4 = new Circulo(c1);
        System.out.println(c1);
        System.out.println(c4);
        Circulo c5 = c2.clone();
        System.out.println(c2);
        System.out.println(c5);
    }
}
