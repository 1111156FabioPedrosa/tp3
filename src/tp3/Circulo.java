/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

/**
 *
 * @author fabio
 */
public class Circulo extends Figura implements Cloneable{

    private double raio;

    public Circulo() {
        this(1);
    }

    public Circulo(double raio) {
        this(raio, "");
    }

    public Circulo(double raio, String corPreenchimento) {
        super(corPreenchimento);
        setRaio(raio);
    }

    public Circulo(Circulo circulo) {
        this(circulo.getRaio(), circulo.getCorPreenchimento());
    }

    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        if (raio < 1) {
            this.raio = 1;
        } else {
            this.raio = raio;
        }
    }

    @Override
    public String toString() {
        return "Círculo com raio " + this.raio + " " + super.toString() + ".";
    }

    @Override
    public double area() {
        return Math.PI * Math.pow(raio, 2);
    }

    @Override
    protected Circulo clone() throws CloneNotSupportedException {
        Circulo cloned = (Circulo) super.clone();
        cloned.setCorPreenchimento(this.getCorPreenchimento());
        cloned.setRaio(this.getRaio());
        return cloned;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Circulo)) {
            return false;
        }
        if (!(this.raio == ((Circulo) obj).getRaio())) {
            return false;
        }
        return true;
    }
    
    
}
